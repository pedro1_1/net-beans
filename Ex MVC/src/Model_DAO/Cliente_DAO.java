/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_DAO;

import Model_bean.Cliente;
import Connection.Conexao;
import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class Cliente_DAO {
    Connection con = null;
    
    public boolean inserir(Cliente c1) throws SQLException, ClassNotFoundException{
        boolean inseriu = false;
        
        try{
            con = new Conexao().getConnection();
            String sql = "INSERT INTO Cliente (Idade, Nome) values (?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, c1.getIdade());
            stmt.setString(2, c1.getNome());
            stmt.execute();
            stmt.close();
            inseriu = true;
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            con.close();
        }
        return inseriu;
    }

    public boolean excluir(Cliente c1) throws SQLException {
        
        boolean exclui = false;
        
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "DELETE FROM Cliente WHERE Id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, c1.getId());
            stmt.execute();
            stmt.close();
            exclui = true;
        } catch(Exception ex) {
            ex.printStackTrace();
        } 
        finally{
            con.close();
        }
        return exclui;
    }
    
    public ArrayList<Cliente> busca() throws SQLException, ClassNotFoundException{
        ArrayList<Cliente> lista;
        lista = new ArrayList<Cliente>();
        ResultSet rs;
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "select * from Cliente";
            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                rs = stmt.executeQuery();
                while(rs.next()){
                    Cliente c1 = new Cliente();
                    c1.setId(rs.getInt("Id"));
                    c1.setIdade(rs.getInt("Idade"));
                    c1.setNome(rs.getString("Nome"));
                    lista.add(c1);
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }finally{
            con.close();
        }
        return lista;
    }
}
