/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Model_DAO.Cliente_DAO;
import Model_bean.Cliente;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class Ctn_cliente {
    public boolean adiciona_cliente(String nome, int idade) throws SQLException, ClassNotFoundException{
        Cliente c1 = new Cliente(nome, idade);
        Cliente_DAO c1DAO = new Cliente_DAO();
        boolean inseriu = c1DAO.inserir(c1);
        return inseriu;
    }
    
    public boolean exclui_cliente(String nome, int idade) throws SQLException{
        Cliente c1 = new Cliente(nome, idade);
        Cliente_DAO c1DAO = new Cliente_DAO();
        boolean exclui = c1DAO.excluir(c1);
        return exclui;
    }
    
    public ArrayList busca_cliente() throws SQLException, ClassNotFoundException {
        Cliente_DAO c1DAO = new Cliente_DAO();
        ArrayList lista = c1DAO.busca();
        return lista;
    }
}
