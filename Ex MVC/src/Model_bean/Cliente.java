/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_bean;

/**
 *
 * @author aluno
 */
public class Cliente {

    private String nome;
    private int idade;
    private int id;
    
    public Cliente(){
        
    }
    
    public Cliente(String nome, int idade){
        this.nome = nome;
        this.idade = idade;
        this.id = 0;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
