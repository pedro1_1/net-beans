/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author aluno
 */
import java.util.ArrayList;
import java.util.Iterator;
import model.Atleta;

public class Controla_atleta {
    private ArrayList atletas;
    
    public Controla_atleta(){
        atletas = new ArrayList();
    }
    
    public void adicionanaLista(String nome, int idade){
        Atleta a1 = new Atleta();
        a1.setNome(nome);
        a1.setIdade(idade);
        this.atletas .add(a1);
    }
    
    public int somaIdade(){
        int sIdade = 0;
        Iterator it = atletas.iterator();
        while (it.hasNext()){
            Atleta a1 = (Atleta)it.next();
            sIdade += a1.getIdade();
        }
        return sIdade;
    }
}
