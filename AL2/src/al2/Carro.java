/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al2;

/**
 *
 * @author aluno
 */
public class Carro {
    private String marca;
    private float potencia;
    
    public Carro(String m, float p){
        marca = m;
        potencia = p;
    }
    
    public Carro(String m){
        marca = m;
        potencia = 1.0f;
    }
    
    public Carro(float p){
        marca = "ford";
        potencia = p;
    }
        
    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the potencia
     */
    public float getPotencia() {
        return potencia;
    }

    /**
     * @param potencia the potencia to set
     */
    public void setPotencia(float potencia) {
        this.potencia = potencia;
    }
    
}
