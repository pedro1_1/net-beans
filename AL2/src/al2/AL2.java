/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al2;

/**
 *
 * @author aluno
 */
public class AL2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Carro c1 = new Carro("Fiat");
        Carro c2 = new Carro(2.0f);
        Carro c3 = new Carro("Honda", 1.8f);
        
        Retangulo r1 = new Retangulo(20, false);
        System.out.println(r1.getAltura() + " - " + r1.getBase());
        System.out.println(r1.area());
        System.out.println(r1.perimetro());
    }
    
}
