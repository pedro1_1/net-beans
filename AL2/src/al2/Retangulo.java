/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package al2;

/**
 *
 * @author aluno
 */
public class Retangulo {
    private float base;
    private float altura;
    
    public Retangulo(float b, float a){
        if ((b>0)&&(b<=20))
            base = b;
        if ((a>0)&&(a<=20))
            altura = a;
    }
    
    public Retangulo(float l, boolean a){
        if ((l>0)&&(l<=20)){
            if (a==true){
                base = 10;
                altura = l;
            } else if(a==false){
                base = l;
                altura = 10;
            }
        }
    }
    
    /**
     * @return the base
     */
    public float getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(float base) {
        this.base = base;
    }

    /**
     * @return the altura
     */
    public float getAltura() {
        return altura;
    }

    /**
     * @param altura the altura to set
     */
    public void setAltura(float altura) {
        this.altura = altura;
    }
    
    public float perimetro(){
        float perimetro;
        perimetro = altura*2+base*2;
        return perimetro;
    }
    
    public float area(){
        float area;
        area = base*altura;
        return area;
    }


}
