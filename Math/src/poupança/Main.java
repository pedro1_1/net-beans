/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poupança;

import static poupança.Poupança.ModificaTaxaJuro;

/**
 *
 * @author aluno
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Poupança p1 = new Poupança(2000.00f);
        Poupança p2 = new Poupança(3000.00f);
        float aux, aux2;
        ModificaTaxaJuro(0.03f);
        p1.caljurMensais();
        aux = p1.getSaldo();
        p2.caljurMensais();
        aux2 = p2.getSaldo();
        System.out.println("Saldo p1 = " + aux);
        System.out.println("Saldo p2 = " + aux2 + "\n");
        ModificaTaxaJuro(0.04f);
        p1.caljurMensais();
        aux = p1.getSaldo();
        p2.caljurMensais();
        aux2 = p2.getSaldo();
        System.out.println("Saldo p1 = " + aux);
        System.out.println("Saldo p2 = " + aux2);
        // TODO code application logic here
    }
    
}
