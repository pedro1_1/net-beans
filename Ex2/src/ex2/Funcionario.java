/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex2;

/**
 *
 * @author aluno
 */
public class Funcionario {
    private String nome;
    private String sobrenome;
    private float salario;
    
    public Funcionario(){
        nome = "";
        sobrenome = "";
        salario = 0;
    }
    
    public Funcionario(String nome, String sobrenome, float salario){
        this.nome = nome;
        this.sobrenome = sobrenome;
        if(salario<0){
            this.salario = 0;
            System.out.println("O salário deve ser positivo");
        }else{
            this.salario = salario;
        }
    }
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the sobrenome
     */
    public String getSobrenome() {
        return sobrenome;
    }

    /**
     * @param sobrenome the sobrenome to set
     */
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    /**
     * @return the salario
     */
    public float getSalario() {
        return salario;
    }

    /**
     * @param salario the salario to set
     */
    public void setSalario(float salario) {
        if(salario<0){
            this.salario = 0;
            System.out.println("O salário deve ser positivo");
        }else{
            this.salario = salario;
        }
    }
    
    public void aumento(Funcionario f,float por){
        float s;
        s = f.salario;
        s += s*(por/100);
        f.salario = s;
    }
}
