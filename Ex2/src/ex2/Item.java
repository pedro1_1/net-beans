/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex2;

/**
 *
 * @author aluno
 */
public class Item {
    private int codigo;
    private int qci;
    private String descricao;
    private float preco;
    
    public Item(int codigo, int qci, String descricao, float preco){
        this.codigo = codigo;
        this.descricao = descricao;
        if(qci<0){
            this.qci = 0;
        }else{
            this.qci = qci;
        }
        if(preco<0){
            this.preco = 0;
        }else{
            this.preco = preco;
        }
    }
    
    public float getTotal(Item i){
        float t;
        if(i.preco < 0){
            i.setPreco(0f);
        }
        if(i.qci < 0){
            i.setQci(0);
        }
        t = i.preco*i.qci;
        return t;
    }
    
    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the qci
     */
    public int getQci() {
        return qci;
    }

    /**
     * @param qci the qci to set
     */
    public void setQci(int qci) {
        this.qci = qci;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the preco
     */
    public float getPreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setPreco(float preco) {
        this.preco = preco;
    }
    
}
