/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex2;

/**
 *
 * @author aluno
 */
public class Pessoa {
    //data
    private int idade;
    private int dia;
    private int mes;
    private int ano;
    private String nome;
    
    //Constructor
    public Pessoa(int dia, int mes, int ano){
        if ((dia<1)||(31>dia)){
            this.dia = 19;
        }else{
            this.dia = dia;
        }
        
        if ((mes<1)||(12>mes)){
            this.mes = 8;
        }else{
            this.mes = mes;
        }
        
        if (ano<1000){
            this.ano = 1000;
        }else{
            this.ano = ano;
        }
    }
    
    public Pessoa(int dia, int mes, int ano, String nome){
        if ((dia<1)||(31>dia)){
            this.dia = 19;
        }else{
            this.dia = dia;
        }
        
        if ((mes<1)||(12>mes)){
            this.mes = 8;
        }else{
            this.mes = mes;
        }
        
        if (ano<1000){
            this.ano = 1000;
        }else{
            this.ano = ano;
        }
        
        this.nome = nome;
    }
    
    //Metodos
    public void calculaIdade(){
        int diac = 18, mesc = 3, anoc = 2019;
        int idadec;
        
        idadec = anoc - this.ano;
        
    }
    
    //Get e Set
    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }
 

    /**
     * @return the dian
     */
    public int getDian() {
        return dian;
    }

    /**
     * @param dian the dian to set
     */
    public void setDian(int dian) {
        this.dian = dian;
    }

    /**
     * @return the mesn
     */
    public int getMesn() {
        return mesn;
    }

    /**
     * @param mesn the mesn to set
     */
    public void setMesn(int mesn) {
        this.mesn = mesn;
    }

    /**
     * @return the anon
     */
    public int getAnon() {
        return anon;
    }

    /**
     * @param anon the anon to set
     */
    public void setAnon(int anon) {
        this.anon = anon;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
    
}
