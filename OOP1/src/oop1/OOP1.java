/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop1;

/**
 *
 * @author aluno
 */
public class OOP1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Carro c1 = new Carro();
        
        c1.setCor("Amarelo");
        c1.setModelo("Corsa");
        c1.setMotor(2.0f);
        c1.acelerar();
        c1.parar();
        
        Carro c2 = new Carro();
        
        c2.setCor("preto");
        c2.setModelo("Fusca");
        c2.setMotor(1.5f);
        c2.acelerar();
        
        Caneta bic1 = new Caneta();
        
        bic1.setCor("Azul");
        bic1.setMarca("bic");
        bic1.setTampada(false);
        bic1.tampar();
        bic1.escrever();
        
    }
    
}
