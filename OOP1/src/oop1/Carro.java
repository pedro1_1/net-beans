/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop1;

/**
 *
 * @author aluno
 */
public class Carro {
    //Atributos
    private String cor;
    private float motor;
    private String modelo;
    
    //Get/Set
    public String getCor(){
        return cor;
    }
    
    public void setCor(String cor){
        this.cor=cor;
    }
    
    public float getMotor(){
        return motor;
    }
    
    public void setMotor(float motor){
        this.motor = motor;
    }
    
    public String getModelo(){
        return modelo;
    }
    
    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    
    //Métodos
    public void acelerar(){
            System.out.println("Acelerando");
    }
    
    public void parar(){
            System.out.println("Parando");
    }
}
