/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop1;

/**
 *
 * @author aluno
 */
public class Caneta {
    
    //atributos
    private boolean tampada;
    private String cor;
    private String marca;
    
    //métodos get/set
    public boolean getTampada(){
        return tampada;
    }
    
    public void setTampada(boolean tampada){
        this.tampada=tampada;
    }
    
    public String getCor(){
        return cor;
    }
    
    public void setCor(String cor){
        this.cor=cor;
    }
    
    public String getMarca(){
        return marca;
    }
    
    public void setMarca(String marca){
        this.marca=marca;
    }
    
    //métodos
    public void tampar(){
        tampada = true;
    }
    
    public void destampar(){
        tampada = false;
    }
    
    public void escrever(){
        if (tampada==true)
            System.out.println("Erro. A caneta está tampada");
        else
            System.out.println("Escrevendo");
    }
}   
