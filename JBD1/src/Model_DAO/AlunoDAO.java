/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_DAO;

import Conection.Conexao;
import Model_bean.Aluno;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class AlunoDAO {

    Connection con = null;
    
    public boolean inserir(Aluno a1) throws SQLException {
        
        boolean inseriu = false;
        
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "INSERT INTO Aluno (ra, Nome) values(?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, a1.getRa());
            stmt.setString(2, a1.getNome());
            stmt.execute();
            stmt.close();
            inseriu = true;
        } catch(Exception ex) {
            ex.printStackTrace();
        } 
        finally{
            con.close();
        }
        return inseriu;
    }
    
    public boolean excluir(Aluno a1) throws SQLException {
        
        boolean exclui = false;
        
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "DELETE FROM Aluno WHERE ra = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, a1.getRa());
            stmt.execute();
            stmt.close();
            exclui = true;
        } catch(Exception ex) {
            ex.printStackTrace();
        } 
        finally{
            con.close();
        }
        return exclui;
    }
    
    public ArrayList<Aluno> buscar() throws SQLException{
        ResultSet r1 = null;
        ArrayList<Aluno> a1 = new ArrayList<Aluno>();
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "SELECT * FROM Aluno";
            PreparedStatement stmt = con.prepareStatement(sql);
            r1 = stmt.executeQuery();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
