/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_bean;

/**
 *
 * @author aluno
 */
public class Aluno {
    private int ra;
    private String nome;
    
    public Aluno(int Ra, String Nome){
        this.ra = Ra;
        this.nome = Nome;
    }
    
    public int getRa() {
        return ra;
    }

    public void setRa(int ra) {
        this.ra = ra;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
