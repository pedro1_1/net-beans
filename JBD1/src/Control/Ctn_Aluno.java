/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Model_DAO.AlunoDAO;
import Model_bean.Aluno;
import java.sql.SQLException;

/**
 *
 * @author aluno
 */
public class Ctn_Aluno {
    
    public boolean adiciona_aluno(int ra, String nome) throws SQLException{
        Aluno a1 = new Aluno(ra, nome);
        AlunoDAO a1DAO = new AlunoDAO();
        boolean inseriu = a1DAO.inserir(a1);
        return inseriu;
    }
    
    public boolean exclui_aluno(int ra, String nome) throws SQLException{
        Aluno a1 = new Aluno(ra, nome);
        AlunoDAO a1DAO = new AlunoDAO();
        boolean exclui = a1DAO.inserir(a1);
        return exclui;
    }
}
