/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestaFuncionarios;

/**
 *
 * @author aluno
 */
public class Gerente extends FUNCIONARIO{
    private String nomeUsuario;
    private String senha;
    
    public Gerente(float salario, String nome, String nomeU, String senha){
        super(salario, nome);
        this.nomeUsuario = nomeU;
        this.senha = senha;
    }
    
    /**
     * @return the nomeUsuario
     */
    public String getNomeUsuario() {
        return nomeUsuario;
    }

    /**
     * @param nomeUsuario the nomeUsuario to set
     */
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    //Metodo
    
    /**
     *
     */
    public void CalculoBonificacao(){
        salario = salario * 0.2f + salario;
    }
}
