/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestaFuncionarios;

/**
 *
 * @author aluno
 */
public class Telefonista extends FUNCIONARIO {
    private int CodEstTrab;
    
    public Telefonista(float salario, String nome, int cet){
        super(salario, nome);
        CodEstTrab = cet;
    }
    
    /**
     * @return the CodEstTrab
     */
    public int getCodEstTrab() {
        return CodEstTrab;
    }

    /**
     * @param CodEstTrab the CodEstTrab to set
     */
    public void setCodEstTrab(int CodEstTrab) {
        this.CodEstTrab = CodEstTrab;
    }
}
