/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestaFuncionarios;

/**
 *
 * @author aluno
 */
public class Secretaria extends FUNCIONARIO {
    private int nRamal;
    
    public Secretaria(float salario, String nome, int ramal){
        super(salario, nome);
        this.nRamal = ramal;
    }
    
    /**
     * @return the nRamal
     */
    public int getnRamal() {
        return nRamal;
    }

    /**
     * @param nRamal the nRamal to set
     */
    public void setnRamal(int nRamal) {
        this.nRamal = nRamal;
    }
}
