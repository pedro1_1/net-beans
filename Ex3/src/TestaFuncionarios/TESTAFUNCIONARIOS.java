/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestaFuncionarios;

/**
 *
 * @author aluno
 */
public class TESTAFUNCIONARIOS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Gerente g1 = new Gerente(10000f, "Claudio Anacleto", "cl4u", "astros2020");
        System.out.println("Gerente N°1: " + g1.getSalario() + ", " + g1.getNome() + ", " + g1.getNomeUsuario() + ", " + g1.getSenha());
        Telefonista t1 = new Telefonista(1000f, "Sandra", 15);
        System.out.println("Telefonista N°1: " + t1.getSalario() + ", " + t1.getNome() + ", " + t1.getCodEstTrab());
        Secretaria s1 = new Secretaria(1000f, " Suzi", 10);
        System.out.println("Secretaria N°1: " + s1.getSalario() + ", " + s1.getNome() + ", " + s1.getnRamal());
        System.out.println("Salario do gerente N°1: " + g1.getSalario());
        g1.CalculoBonificacao();
        System.out.println("Novo salario do gerente N°1: " + g1.getSalario());
        System.out.println("Salario da telefonista N°1: " + t1.getSalario());
        t1.CalculaBonificacao();
        System.out.println("Novo salario da telefonista N°1: " + t1.getSalario());
        System.out.println("Salario da secretaria N°1: " + s1.getSalario());
        s1.CalculaBonificacao();
        System.out.println("Novo salario da secretaria N°1: " + s1.getSalario());
    }
    
}
