/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestaFuncionarios;

/**
 *
 * @author aluno
 */
public class FUNCIONARIO {
    protected float salario;
    private String nome;
    
    public FUNCIONARIO(float salario, String nome){
        this.nome = nome;
        this.salario = salario;
    }
    
    /**
     * @return the salario
     */
    public float getSalario() {
        return salario;
    }

    /**
     * @param salario the salario to set
     */
    public void setSalario(float salario) {
        this.salario = salario;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    //Metodos
    
    public void CalculaBonificacao(){
         salario = salario * 0.1f + salario;
    }
    
    public void Mostradados(){
        System.out.println("Nome: " + nome);
        System.out.println("Salario: " + salario);
    }
}
