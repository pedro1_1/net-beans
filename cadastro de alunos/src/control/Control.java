/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;
import java.util.ArrayList;
import java.util.Iterator;
import model.Aluno;

/**
 *
 * @author aluno
 */
public class Control {
    private ArrayList alunos;
    
    public Control(){
        alunos = new ArrayList();
    }
    
    public void adiciona_aluno(String nome, int idade, float altura, String status){
        Aluno a1 = new Aluno();
        a1.setNome(nome);
        a1.setIdade(idade);
        a1.setAltura(altura);
        a1.setStatus(status);
        this.alunos.add(a1);
    }
    
    public float media_idade(){
        int soma = 0;
        int i=0;
        float media;
        Iterator it = alunos.iterator();
        while(it.hasNext()){
            Aluno a1 = (Aluno)it.next();
            soma += a1.getIdade();
            i++;
        }
        media = soma/i;
        return media;
    }
    
    public float maior_altura(){
        float altura = 0;
        Iterator it = alunos.iterator();
        while(it.hasNext()){
            Aluno a1 = (Aluno)it.next();
            if (a1.getAltura()>altura)
                altura = a1.getAltura();
        }
        return altura;
    }
}
