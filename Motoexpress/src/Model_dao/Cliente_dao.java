/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_dao;

import Connection.Conexao;
import Model_bean.Cliente;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class Cliente_dao {
    Connection con = null;
    
    public boolean inserir(Cliente c) throws SQLException{
        boolean add = false;
        
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "insert into Cliente (CPF, Nome, Endereco, Telefone, E-Mail) values (?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, c.getCPF());
            stmt.setString(2, c.getNome());
            stmt.setString(3, c.getEndereco());
            stmt.setString(4, c.getTelefone());
            stmt.setString(5, c.getEmail());
            stmt.execute();
            stmt.close();
            add=true;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Cliente_dao.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            con.close();
        }
        return add;
    }
}
