/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_bean;

/**
 *
 * @author aluno
 */
public class Cliente {
    private int CPF;
    private String nome, endereco, telefone, email;
    
    public Cliente(int cpf, String nome, String endereco, String telefone, String email){
        this.CPF = cpf;
        this.email = email;
        this.endereco = endereco;
        this.nome = nome;
        this.telefone = telefone;
    }
    
    public Cliente(int cpf){
        this.CPF = cpf;
        this.email = null;
        this.endereco = null;
        this.nome = null;
        this.telefone = null;
    }
    
    public Cliente(int cpf, String u, int i){
        this.CPF = cpf;
        switch(i){
            case 1:{
                this.email = u;
                this.endereco = null;
                this.nome = null;
                this.telefone = null;
            }
            case 2: {
                this.email = null;
                this.endereco = u;
                this.nome = null;
                this.telefone = null;
            }
            case 3:{
                this.email = null;
                this.endereco = null;
                this.nome = u;
                this.telefone = null;
            }
            case 4:{
                this.email = null;
                this.endereco = null;
                this.nome = null;
                this.telefone = u;
            }
        }
    }
    
    public int getCPF() {
        return CPF;
    }

    public void setCPF(int CPF) {
        this.CPF = CPF;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
