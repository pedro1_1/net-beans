package br.com.cotilbr.aula.usuario;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@Service
public class UsuarioService {
    
    @Autowired
    UsuarioRepository usuarioRepository;
    
    public List<UsuarioModel> getUsuario(){       
        return usuarioRepository.findAll();
    }

    @PostMapping
    public UsuarioModel postUsuario(UsuarioModel usuarioModel) {
        if(usuarioModel.getIdade()<16){
            return null;
        } else{
            return usuarioRepository.save(usuarioModel);
        }
    }
      
}
