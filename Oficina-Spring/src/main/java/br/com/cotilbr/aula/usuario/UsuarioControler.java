package br.com.cotilbr.aula.usuario;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioControler {
    
    @Autowired
    UsuarioService usuarioService;
    
    @GetMapping
    public List<UsuarioModel> getUsuario(){
        return usuarioService.getUsuario();       
    }
    
    @PostMapping
    public void postUsuario(@RequestBody UsuarioModel usuarioModel){
        usuarioService.postUsuario(usuarioModel);
    }
}
