/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Model_DAO.CarroDAO;
import Model_bean.Carro;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class Ctn_carro {
    
    public boolean adiciona_carro(String placa, String marca, String modelo, int ano, String cor) throws SQLException{
        Carro c1 = new Carro(placa, marca, modelo, cor, ano);
        CarroDAO c1DAO = new CarroDAO();
        boolean inseriu = c1DAO.inserir(c1);
        return inseriu;
    }
    
    public boolean exclui_carro(String placa) throws SQLException{
        Carro c1 = new Carro(placa);
        CarroDAO c1DAO = new CarroDAO();
        boolean exclui = c1DAO.excluir(c1);
        return exclui;
    }
    
    public ArrayList busca() throws SQLException, ClassNotFoundException{
        CarroDAO c1DAO = new CarroDAO();
        ArrayList c1 = c1DAO.busca();
        return c1;
    }
}
