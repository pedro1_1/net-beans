/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model_DAO;

import Connection.Conexao;
import Model_bean.Carro;
import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class CarroDAO {
    Connection con = null;
    
    public boolean inserir(Carro c1) throws SQLException{
        boolean inseriu = false;
        
        try{
            con = new Conexao().getConnection();
            String sql = "INSERT INTO Veiculos (Placa, Marca, Ano, Modelo, Cor) values (?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, c1.getPlaca());
            stmt.setString(2, c1.getMarca());
            stmt.setInt(3, c1.getAno());
            stmt.setString(4, c1.getModelo());
            stmt.setString(5, c1.getCor());
            stmt.execute();
            stmt.close();
            inseriu = true;
        } catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            con.close();
        }
        return inseriu;
    }
    
    public boolean excluir(Carro c1) throws SQLException {
        
        boolean exclui = false;
        
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "DELETE FROM Veiculos WHERE Placa = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, c1.getPlaca());
            stmt.execute();
            stmt.close();
            exclui = true;
        } catch(Exception ex) {
            ex.printStackTrace();
        } 
        finally{
            con.close();
        }
        return exclui;
    }
    
    public ArrayList<Carro> busca() throws SQLException, ClassNotFoundException{
        ArrayList<Carro> lista;
        lista = new ArrayList<Carro>();
        ResultSet rs;
        try{
            con = (Connection) new Conexao().getConnection();
            String sql = "select * from Veiculos";
            PreparedStatement stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while(rs.next()){
                Carro c1 = new Carro();
                c1.setPlaca(rs.getString("Placa"));
                c1.setModelo(rs.getString("Modelo"));
                c1.setMarca(rs.getString("Marca"));
                c1.setCor(rs.getString("Cor"));
                c1.setAno(rs.getInt("Ano"));
                lista.add(c1);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally{
            con.close();
        }
        return lista;
    }
}
