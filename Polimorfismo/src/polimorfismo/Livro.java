/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author aluno
 */
public class Livro extends Produto {
   
    /**
     *
     */
    @Override
    public void mostraDados(){
        System.out.println("Dados do livro");
    }
    
    public void mostraAutor(){
        System.out.println("Dados do autor");
    }
}
