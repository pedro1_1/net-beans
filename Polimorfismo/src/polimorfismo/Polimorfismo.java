/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author aluno
 */
public class Polimorfismo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Produto p1 = new Produto();
        p1.mostraDados();
        Produto p2 = new Livro();
        p2.mostraDados();
        Livro l2 = (Livro) p2;
        l2.mostraAutor();
        Produto p3 = new Revista();
        p3.mostraDados();
        
    }
    
}
