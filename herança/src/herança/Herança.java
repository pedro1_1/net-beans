/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herança;

/**
 *
 * @author aluno
 */
public class Herança {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Aluno al1 = new Aluno("presunto", "654.562.843-18", 75134);
        al1.imprime();
        Professor p1 = new Professor("fulaninho", "371.565.513-95", 20f);
        p1.imprime();
        Pessoa p2 = new Pessoa("Weidson", "545.264.543-65");
        p2.imprime();
    }
    
}
