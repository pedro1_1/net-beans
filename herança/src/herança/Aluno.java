/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herança;

/**
 *
 * @author aluno
 */
public class Aluno extends Pessoa {
    private int ra;
    
    public Aluno(String nome, String CPF, int ra){
        super(nome, CPF);
        this.ra = ra;
    }
    
    @Override
    public void imprime(){
        //System.out.println("Nome: " + this.nome);
        //System.out.println("CPF: " + this.CPF);
        super.imprime();
        System.out.println("RA: " + this.ra);
    }
}

