/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herança;

/**
 *
 * @author aluno
 */
public class Pessoa {
    private String nome;
    private String CPF;
    
    public Pessoa(String nome, String cpf){
        this.nome = nome;
        CPF = cpf;
    }
    
    public void imprime(){
        System.out.println("Nome: " + this.nome);
        System.out.println("CPF: " + this.CPF);
    }
}
