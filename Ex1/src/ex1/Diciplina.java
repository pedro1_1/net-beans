/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author aluno
 */
public class Diciplina {

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    /**
     * @return the carga_horaria
     */
    public int getCarga_horaria() {
        return carga_horaria;
    }

    /**
     * @param carga_horaria the carga_horaria to set
     */
    public void setCarga_horaria(int carga_horaria) {
        this.carga_horaria = carga_horaria;
    }

    /**
     * @return the Sala
     */
    public int getSala() {
        return Sala;
    }

    /**
     * @param Sala the Sala to set
     */
    public void setSala(int Sala) {
        this.Sala = Sala;
    }
    
    private int ID;
    private String Nome;
    private int carga_horaria;
    private int Sala;
    
    public void Inicio_de_aula(){
        System.out.println("A(s) Aula(s) começou(aram)");
    }
}
