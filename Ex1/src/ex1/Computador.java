/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author aluno
 */
public class Computador {

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the estado_PC
     */
    public boolean isEstado_PC() {
        return estado_PC;
    }

    /**
     * @param estado_PC the estado_PC to set
     */
    public void setEstado_PC(boolean estado_PC) {
        this.estado_PC = estado_PC;
    }
    
    private int codigo;
    private boolean estado_PC = false;
    
    public void Ligar(){
        setEstado_PC(true);
    }
    
    public void Desligar(){
        setEstado_PC(false);
    }
}
