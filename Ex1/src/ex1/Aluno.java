/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author aluno
 */
public class Aluno {
    //Variaveis
    private int RA;
    private String Nome;
    private int idade;
    private int RG;
    private float nota;
    
    //construtor
    public Aluno(int ra, String no, int i, int rg, float nt){
        if (ra>0)
            RA = ra;
        Nome = no;
        if (i>=0)
        idade = i;
        if (rg>0)
        RG = rg;
        if ((nt<=10)&&(nt>=0))
        nota = nt;
    }
    
    public Aluno(int ra, String nome){
        if (ra>0)
            RA = ra;
        Nome = nome;
        idade = 0;
        RG = 0;
        nota = 0f; 
    }
    
    public Aluno(){}
    
    //Métodos get/set
    public int getRA(){
	return RA;
    }   
    
    public void setRA(int RA){
	this.RA = RA;
    }
    
    public String getNome(){
	return Nome;
    }
    
    public void setNome(String Nome){
	this.Nome = Nome;
    }
    
    public int getIdade(){
	return idade;
    }
    
    public void setIdade(int idade){
	this.idade = idade;
    }
    
    public int getRG(){
	return RG;
    }
    
    public void setRG(int RG){
	this.RG = RG;
    }  
    
    public float getNota(){
	return nota;
    }
    
    public void setnota(float nota){
	this.nota = nota;
    }
    
    //Métodos
    public void Estudar(){
        System.out.println("Aluno Estudando");
    }
    
    public void Liberar(){
        System.out.println("Aluno Liberado");
    }
    
    public void Aprovar(){
        if (nota<6.0f)
            System.out.println("Reprovado");
        else
            System.out.println("Aprovado");
    }
    
}
