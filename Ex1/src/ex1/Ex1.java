/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author aluno
 */
public class Ex1 {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Aluno a1 = new Aluno(17604, "Maria", 17, 55795431, 10);
        System.out.println(a1.getNome() + " - " + a1.getIdade() + " - " + a1.getRA() + " - " + a1.getRG() + " - " + a1.getNota());
        a1.setNome("Jão");
        a1.setIdade(17);
        System.out.println(a1.getNome() + " - " + a1.getIdade());
        Aluno a2 = new Aluno(17605, "Carlos");
        System.out.println(a2.getNome() + " - " + a2.getRA());
    }
    
}
